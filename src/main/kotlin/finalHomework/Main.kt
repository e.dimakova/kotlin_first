package finalHomework

import finalHomework.ShipState.EMPTY

val playDesc = mutableMapOf(
    "A1" to EMPTY,
    "A2" to EMPTY,
    "A3" to EMPTY,
    "A4" to EMPTY,
    "B1" to EMPTY,
    "B2" to EMPTY,
    "B3" to EMPTY,
    "B4" to EMPTY,
    "C1" to EMPTY,
    "C2" to EMPTY,
    "C3" to EMPTY,
    "C4" to EMPTY,
    "D1" to EMPTY,
    "D2" to EMPTY,
    "D3" to EMPTY,
    "D4" to EMPTY
)

val playDescTwo : MutableMap<String, ShipState> = HashMap(playDesc)

fun playGame(player1: Player, player2: Player) {
    var win = false
    while (!win) {
        player1.hitShip(playDescTwo)
        player2.hitShip(playDesc)
        win = player1.winOrNot() || player2.winOrNot()
    }
    if (player1.winOrNot()) {
        println("Выйграл игрок ${player1.getName()}")
    }
    if (player2.winOrNot()) {
        println("Выйграл игрок ${player2.getName()}")
    }
}

fun main() {

    println("Введите имя первого игрока:")
    val nameFirstPlayer = readLine()
    val shipOfFirstPlayer = Ship(playDesc)
    shipOfFirstPlayer.setAllShip(playDesc)
    val playerOne = Player(nameFirstPlayer)

    println("Введите имя второго игрока:")
    val nameSecondPlayer = readLine()
    val shipOfSecondPlayer = Ship(playDescTwo)
    shipOfSecondPlayer.setAllShip(playDescTwo)
    val playerTwo = Player(nameSecondPlayer)

    playGame(playerOne, playerTwo)
}
