package finalHomework

enum class ShipState {
    EMPTY,
    ONE_SHIP,
    TWO_SHIP,
    HURT_SHIP,
    KILL_SHIP
}
