package finalHomework

class Ship(private var play: MutableMap<String, ShipState>) {

    private val oneShipPlayer = mutableSetOf<String>()

    fun setAllShip(play: MutableMap<String, ShipState>) {
        println("Задайте поле для первого однопалубного корабля:")
        val ship1 = createShip()
        println("Задайте поле для второго однопалубного корабля:")
        val ship2 = createShip()
        println("Задайте поле для первой части однопалубного корабля:")
        val ship3 = createShip()
        val set = findAvailableField(ship3)
        val ship4 = setSecondField(set)
        println("Все корабли успешно введены: однопалубные $ship1 и $ship2 ; двупалубный [$ship3 , $ship4].")
    }

    private fun createShip(): String {
        var ship: String = readLine()!!
        while (!play.containsKey(ship) || oneShipPlayer.contains(ship)) {
            println("Поле введено неверно. Попробуйте еще раз")
            ship = readLine()!!
        }
        oneShipPlayer.add(ship)
        play[ship] = ShipState.ONE_SHIP
        return ship
    }

    private fun findAvailableField(firstPart: String): MutableSet<String> {
        play[firstPart] = ShipState.TWO_SHIP

        val letter = arrayOf("A", "B", "C", "D")
        val numbers = arrayOf("1", "2", "3", "4")
        val setTwo = mutableSetOf<String>()

        val a1 = letter.indexOf(firstPart.toCharArray()[0].toString())     // индекс буквы первого корабля в массиве letter
        val b1 = numbers.indexOf(firstPart.toCharArray()[1].toString())    // индекс цифры первого корабля в массиве letter

        if (b1 < 3) {
            setTwo.add(letter[a1] + numbers[b1 + 1])
        }
        if (b1 > 0) {
            setTwo.add(letter[a1] + numbers[b1 - 1])
        }
        if (a1 < 3) {
            setTwo.add(letter[a1 + 1] + numbers[b1])
        }
        if (a1 > 0) {
            setTwo.add(letter[a1 - 1] + numbers[b1])
        }
        setTwo.removeAll(oneShipPlayer)
        println("Выберите поле для второй части корабля из $setTwo:")
        return setTwo
    }

    private fun setSecondField(setTwo: MutableSet<String>): String {
        var ship: String = readLine()!!
        while (!setTwo.contains(ship)) {
            println("Поле должно быть выбрано из предложенных вариантов")
            ship = readLine()!!
        }
        oneShipPlayer.add(ship)
        play[ship] = ShipState.TWO_SHIP
        return ship
    }
}
