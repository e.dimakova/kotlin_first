package finalHomework

import finalHomework.ShipState.*

class Player(
   private val name: String?
) {
    private var win = false
     fun getName(): String? {
         return name
     }

    fun winOrNot(): Boolean{
        return win
    }

    private fun checkGoal(playDesc : MutableMap<String, ShipState>): String {
        var goal: String = readLine()!!
        while (!playDesc.containsKey(goal)) {
            println("Поле введено неверно. Попробуйте еще раз")
            goal = readLine()!!
        }
        return goal
    }

    fun hitShip(playDesc : MutableMap<String, ShipState>): Boolean {
        println("Ходит игрок $name")
        val goal = checkGoal(playDesc )
        when (playDesc [goal]) {
            ONE_SHIP -> {
                playDesc [goal] = KILL_SHIP
                win = !(playDesc.containsValue(ONE_SHIP) || playDesc.containsValue(TWO_SHIP))
                println("$goal: Убил")
            }

            TWO_SHIP -> {
                if (playDesc .containsValue(HURT_SHIP)) {
                    playDesc[goal] = KILL_SHIP
                    win = !(playDesc.containsValue(ONE_SHIP) || playDesc.containsValue(TWO_SHIP))
                    println("$goal: Убил")
                } else {
                    playDesc[goal] = HURT_SHIP
                    println("$goal: Ранил")
                }
            }

            EMPTY -> {
                println("$goal: Мимо")
            }

            else -> {
                println("$goal: Эта цель была ранена или убита ранее")
            }
        }
        return win
    }
}
