package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    var fiveMarkCount = 0
    var fourMarkCount = 0
    var threeMarkCount = 0
    var twoMarkCount = 0

    for (item in marks) {
        when (item) {
            2 -> twoMarkCount += 1
            3 -> threeMarkCount += 1
            4 -> fourMarkCount += 1
            5 -> fiveMarkCount += 1
        }
    }

    fun percent(x: Int): Double {
        return x * 1000 / marks.size / 10.0
    }

    println("Отличников - ${percent(fiveMarkCount)}%")
    println("Хорошистов - ${percent(fourMarkCount)}%")
    println("Троечников - ${percent(threeMarkCount)}%")
    println("Двоечников - ${percent(twoMarkCount)}%")
}