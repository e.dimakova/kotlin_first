package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var evenNumber = 0
    for (item in myArray) {
        if (item % 2 == 0) {
            evenNumber += 1
        }
    }
    val oddNumber = myArray.size - evenNumber
    println("Джо забирает себе $evenNumber четных монет")
    println("Команда забирает себе $oddNumber нечетных монет")
}
