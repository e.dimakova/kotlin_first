package homework5

class Lion(
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override val favouriteFood: Array<String> = arrayOf("мясо"),
    fullness: Int = 0
) : Animal(fullness)
