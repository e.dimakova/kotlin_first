package homework5

abstract class Animal(var fullness: Int) {

    abstract val name: String
    abstract val height: Int
    abstract val weight: Int
    abstract val favouriteFood: Array<String>

    fun eat(food: Array<String>) {
        food.forEach {  food ->
            for (j in favouriteFood) {
                if (j == food ) {
                    fullness += 1
                }
            }
        }
    }
}
