package homework5

class Elephant(
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override val favouriteFood: Array<String> = arrayOf("конфеты"),
    fullness: Int = 0
) : Animal(fullness)
