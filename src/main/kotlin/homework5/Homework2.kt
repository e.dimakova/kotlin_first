package homework5

import java.lang.Integer.parseInt
import java.lang.StrictMath.pow

fun main() {
    val digit = readLine()
    parseInt(digit)
    if (digit.isNullOrEmpty()) {
        println("Введено не число")
    } else {
        println(numberReverse(digit.toInt()))
    }
}

private fun findNumberZero(digit: Int): Int { // функция ищет кол-во десятков в числе
    var i = 0
    var y = digit
    while (y > 0) {
        i += 1
        y /= 10
    }
    return i
}

private fun numberToArray(digit: Int, zeroNumber: Int): List<Int> { // переводит число в массив
    var a = zeroNumber
    var b = 0
    var c = digit
    val list = mutableListOf<Int>()
    while (a > 1) {
        a--
        b = c / pow(10.0, a.toDouble()).toInt()
        list += b
        c -= b * pow(10.0, a.toDouble()).toInt()
    }
    list += c
    return list
}

private fun arrayToNumberReverse(array: List<Int>): Int { // переводит из массива в перевернутое число
    var number = array[0]
    for (i in 1 until array.size) {
        number += array[i] * pow(10.0, i.toDouble()).toInt()
    }
    return number
}

fun numberReverse(digit: Int): Int {
    return (arrayToNumberReverse(numberToArray(digit, findNumberZero(digit))))
}
