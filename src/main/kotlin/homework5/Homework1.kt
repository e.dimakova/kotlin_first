package homework5

fun printInfoAboutEat(animal: Array<Animal>, food: Array<String>) {
    for (hungryAnimal in animal)  {
        print("${hungryAnimal.name} был сыт на ${hungryAnimal.fullness} балла. Любит ${hungryAnimal.favouriteFood.contentDeepToString()}.")
        hungryAnimal.eat(food)
        println("Съел ${food.contentDeepToString()} и сытость стала ${hungryAnimal.fullness}")
    }
}
fun main() {
    val animalsList = arrayOf(
        Lion("Лев", 12, 45, arrayOf("мясо", "тушенка", "рыба"), 3),
        Tiger("Тигр", 34, 33, arrayOf("тушенка", "трава"), 1),
        Hippo("Бегемот", 4, 11, arrayOf("трава", "арбуз", "вода"), 2),
        Wolf("Волк", 11, 56, arrayOf("заяц", "кость", "мясо"), 0),
        Giraffe("Жираф", 102, 20, arrayOf(), 9),
        Elephant("Слон", 30, 200),
        Chimpanzee("Шимпанзе", 45, 12, arrayOf("банан", "фрукты"), 4),
        Gorilla("Горилла", 6, 24, arrayOf("банан", "конфета"), 7)
    )

    val foodList = arrayOf("банан", "мясо", "икра", "мясо")
    printInfoAboutEat(animalsList, foodList)
}
