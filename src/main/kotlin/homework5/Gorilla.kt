package homework5

class Gorilla(
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override val favouriteFood: Array<String> = arrayOf("банан"),
    fullness: Int = 0
) : Animal(fullness)
