package homework5

class Tiger(
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override val favouriteFood: Array<String> = arrayOf("молоко"),
    fullness: Int = 0
) : Animal(fullness)
