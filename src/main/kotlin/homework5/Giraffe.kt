package homework5

class Giraffe(
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override val favouriteFood: Array<String> = arrayOf("трава"),
    fullness: Int = 0
) : Animal(fullness)
