package homework7

class WrongPasswordException(message: String) : Exception(message)
class WrongLoginException(message: String) : Exception(message)

fun verification(login: String, password: String, passwordConfirmation: String): User {
    if (login.length > 20) {
        throw WrongLoginException("В логине не должно быть более 20 символов")
    }
    if (password.length < 10) {
        throw WrongPasswordException("В пароле не должно быть менее 10 символов.")
    }
    if (password != passwordConfirmation) {
        throw WrongPasswordException("Пароли должны совпадать.")
    }
    println("Пользователь успешно создан")
    return User(login, password)
}

fun main() {

    println("Введите логин:")
    val login = readLine()
    println("Введите пароль:")
    val password = readLine()
    println("Введите пароль еще раз:")
    val passwordConfirmation = readLine()

    verification(login!!, password!!, passwordConfirmation!!)
}

class User(val login: String, val password: String) {
}
