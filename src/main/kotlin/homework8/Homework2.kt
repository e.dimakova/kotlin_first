package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun countVegetableAtUserCart(userCart: MutableMap<String, Int>, vegetableSet: Set<String>): Int {
    var numberOfVegetables = 0
    vegetableSet.forEach {
        numberOfVegetables += userCart.getValue(it)
    }
    return numberOfVegetables
}

fun newPrices(
    basePrices: MutableMap<String, Double>,
    discountSet: Set<String>,
    discountValue: Double
): MutableMap<String, Double> {
    discountSet.forEach {
        basePrices[it] = basePrices.getValue(it) * (1 - discountValue)
    }
    return basePrices
}

fun priceAll(prices: Map<String, Double>): Double {
    var sum = 0.0
    for ((key, value) in prices) {
        sum += value * userCart.getValue(key)
    }
    return sum
}

fun main() {
    println("Количество овощей в корзине ${countVegetableAtUserCart(userCart, vegetableSet)}")
    println("Сумма товаров ${priceAll(newPrices(prices, discountSet, discountValue))}")
}
