package homework1

fun main() {
    val lemonade = 18500
    val pinaColada: Short = 200
    val whiskey: Byte = 50
    val fresh : Long = 3000000000
    val cola = 0.5F
    val ale: Double = 666666667e-9
    val barmenInspiration: String = "Что-то авторское!"

    val order: String = "Заказ -"
    val isReady: String = "готов!"

    println("$order '$lemonade мл лимонада' $isReady")
    println("$order '$pinaColada мл пина колады' $isReady")
    println("$order '$whiskey мл виски' $isReady")
    println("$order '$fresh капель фреша' $isReady")
    println("$order '$cola литра колы' $isReady")
    println("$order '$ale литра эля' $isReady")
    println("$order '$barmenInspiration' $isReady")
}
